var productList_template = $('body').attr('productList_template');
var left_bar_template = $('body').attr('left_bar_template');
var pagination_template = $('body').attr('pagination_template');
var productdata_template = $('body').attr('productdata_template');
var quicksearch_template = $('body').attr('quicksearch_template');
var compareListArray = []; //store product code and indexes of selected products for comparing
var compare_count = 0; // to store the quantity of products added to compare list
var productListdataStoreBackUp, productListDataCompareList, productFilteredData, current_product_displayed_data;
var response, versionResponse; //stores the JSAN response object
var countLimit = $('body').attr('min_item_limit'); // page count limit
//var pindex = [];
//var pcode = [];
var sourceView2;
var union_array = []; // this array contains the union of all the product 
var intersection_array = []; // this will contatin data based on intersection of all facet values
var current_page_number = 1;
var noOfPages;
var allFacetsProductCodeIndexesUpdatedCount = [];
var indexesNotToUpdate = [];
//var checkedpcodes=[];
var JsanDomain = window.location.origin + "/nv/croma";
var noImageAvailaibleUrl = "http://hmny.nviz.co/nitrogenstorefront/_ui/desktop/theme-blue/images/missing-product-300x300.jpg";
var current_selected_checkbox_count = 0;
var current_checked_indexes_innerouterloopcommaseperatedArray = [];
var unsorted_data = [];
var isflag = false;
var flag_operation = "append";
var append = "_total";
var prepend = "_total";
var replace = "_total";
//var facet_flag_positions=["0,5"];
var facet_flag_behavious = [];
var facet_flag_name = ["Include Out of Stock"];
var clicked_flag = false;
var categorycode = "";
var currentPDPProductcode = "";
var currentPDPProductIndex = "";
var previous_page_checked_fileters_indexes = [];
var previous_page_checked_pcodes = [];
var previous_page_checked_product_names = [];
var current_page_checked_product_names = [];
var globalcategorycode;
var prodArray3d="";
var pageArray=[];
var previous_page_sorting_selected_option="relevance";
// mayank's variable ends here 
// function for fetching JSAN object
function getJSANversion() {
    var url = window.location.href;
    var codeFromUrl = url.substring(url.lastIndexOf("/"));
    var matches = codeFromUrl.match(/^[^#?]*/);
    var code = matches[0];
    categorycode = code.split("/")[1];
    globalcategorycode = categorycode;
    //var code = '/916'
    console.log(JsanDomain + "/catalog/categories" + code + "/latestVersion.json");
    var version_response;
    $.ajax({
        async: false,
        type: 'GET',
        url: JsanDomain + "/catalog/categories" + code + "/latestVersion.json",
        success: function(response) {
            version_response = response;
        },
        complete: function(xhr, textStatus) {
            //console.log(xhr);
        }
    }).done(function(response) {
        //console.log("Inside ajax call to jsan url");
        console.log(response);
    });

    return version_response;
}

function getJSANData(Version_number) {

    var url = window.location.href;
    var codeFromUrl = url.substring(url.lastIndexOf("/"));
    var matches = codeFromUrl.match(/^[^#?]*/);
    var code = matches[0];
    var jsan_url = JsanDomain + "/catalog/categories" + code + "/" + Version_number + code + ".jsan";
    if (isflag) {
        if (flag_operation == "append") {
            jsan_url = JsanDomain + "/catalog/categories" + code + "/" + Version_number + code + append + ".jsan"
        }
        if (flag_operation == "prepend") {
            jsan_url = JsanDomain + "/catalog/categories" + code + "/" + Version_number + prepend + code + ".jsan"
        }
        if (flag_operation == "replace") {

            jsan_url = getReplaced_url();

        }

    }
    var apiResponse;
    console.log("JSAN url  " + jsan_url);
    $.ajax({
        async: false,
        type: 'GET',
        url: jsan_url,
        success: function(responseJsan) {
            apiResponse = responseJsan;
        },
        complete: function(xhr, textStatus) {
            //console.log(xhr);
        }
    }).done(function(responseJsan) {
        //console.log("Inside ajax call to jsan url");
        apiResponse = responseJsan;
        console.log(responseJsan);
    });
    return apiResponse;
}

// pass the object to create following arrays in the object
function createFields(object) {
    object["relevance_results_name"] = [];
    object["relevance_results_images"] = [];
    object["relevance_price_value"] = [];
    object["relevance_results_summary"] = [];
    object["relevance_results_averageRating"] = [];
    object["relevance_results_code"] = [];
    object["PLP_thumbnail_urls"] = [];
    object["PDP_main_image_urls"] = [];
    object["PDP_thumbnail_urls"] = [];
    object["relevance_results_mrp"] = [];
object["stockLevelStatus"] = [];
}


function getCommonElements(arrays) { //Assumes that we are dealing with an array of arrays of integers
    var currentValues = {};
    var commonValues = {};
    for (var i = arrays[0].length - 1; i >= 0; i--) { //Iterating backwards for efficiency
        currentValues[arrays[0][i]] = 1; //Doesn't really matter what we set it to
    }
    for (var i = arrays.length - 1; i > 0; i--) {
        var currentArray = arrays[i];
        for (var j = currentArray.length - 1; j >= 0; j--) {
            if (currentArray[j] in currentValues) {
                commonValues[currentArray[j]] = 1; //Once again, the `1` doesn't matter
            }
        }
        currentValues = commonValues;
        commonValues = {};
    }
    return Object.keys(currentValues).map(function(value) {
        return parseInt(value);
    });
}

function initializeProductListdataStoreBackUp() {
    for (var i = 0; i < response.Result_results_name.length; i++) {
        productListdataStoreBackUp["relevance_results_name"][i] = response.Master_results_name[response.Result_results_name[i]]
        productListdataStoreBackUp["relevance_results_images"][i] = productListdataStoreBackUp.PLP_thumbnail_urls[i];
        if (productListdataStoreBackUp.PLP_thumbnail_urls[i] == null) {
            productListdataStoreBackUp["relevance_results_images"][i] = noImageAvailaibleUrl;
        }
        productListdataStoreBackUp["relevance_price_value"][i] = response.Master_results_price_value[response.Result_results_price_value[i]];
        productListdataStoreBackUp["relevance_results_mrp"][i] = response.Master_results_mrp[response.Result_results_mrp[i]];
        productListdataStoreBackUp["relevance_results_summary"][i] = response.Master_results_summary[response.Result_results_summary[i]];
        productListdataStoreBackUp["relevance_results_averageRating"][i] = response.Master_results_averageRating[response.Result_results_averageRating[i]];
        productListdataStoreBackUp["relevance_results_code"][i] = response.Master_results_code[response.Result_results_code[i]];
productListdataStoreBackUp["stockLevelStatus"][i] = response.Master_results_stock_stockLevelStatus_code[response.Result_results_stock_stockLevelStatus_code[i]];
    }

}

//this method is called after clicking on any product in PLP
function openPDP(code) {
    //console.log("inside PDP");
    console.log("product code is " + code.id);
    var index = productListdataStoreBackUp.relevance_results_code.indexOf($.trim(code.id));
    productindex = index;
    getProductDetails(index);
    getSessionToken();
    $("#replicateATC").append($("#addToCartForm" + code.id).clone(true));
    checkPinCodeOnPDPPage(code.id, index);
    currentPDPProductcode = code.id;
    currentPDPProductIndex = index;
    load3d(code.id, 1);
//$( ".ui-dialog" ).remove();
 replicateCompareBoxOnPDP();
}

// this is for tab navigation in PDP
function openTabPDP(id) {
    load3d(id, globalcategorycode);
    var target_tab = $('#vview' + id).parents('.pContentTab');
    if ($(target_tab).hasClass('active')) {
        $(target_tab).removeClass('active').find('.tabBody').slideUp('slow');
        $(target_tab).find('span').text('+');
    } else {
        $('.pContentTab').removeClass('active').find('.tabBody').slideUp('slow');
        $('.pContentTab').find('span').text('+');
        $(target_tab).addClass('active').find('.tabBody').slideDown('slow');
        $(target_tab).find('span').text('-');
    }
}
//this is for opening review form in PDP
function openReviewForm() {
    //////console.log("review form");
    $(".tabBody").css("display", "none");
    $("#tabBody-1").attr('style', 'display: block !important');
    $("#write_reviews").attr('style', 'display: block !important');

}

function hideProduct(code) {
    addToCompare(code);
    updateComparePage(); //generatePageResults('comparelist-section', 'comparelist_template', productListDataCompareList);
}
// this function is called after user clicks on "Add to compare" on any product
function addToCompare(code) {
    code = $.trim(Number(code));

    // add this code to comparelist array 

    if ($.inArray(code.toString(), compareListArray) == -1) {
           // this code adds an element in the compare list box
        if(compare_count<3) // 4 bcoz croma compare box allows only 3 products maximum
        {// smaller than 3 is bcoz when value enters here we send ++ compare count as it started from 0
        compareListArray.push($.trim(Number(code)));

        show_compare_count(++compare_count);
        $("#" + "btn" + code).css("background-color", "#00bfff");
        $("#" + "btn" + code).text("Remove");
        console.log("changing color of " + code);
       }
    } else {
          // this code removes an element in the compare list box
        //console.log(compareListArray);
        console.log("code is" + code);
        var index = compareListArray.indexOf(code);
        compareListArray.splice(index, 1);
        show_compare_count(--compare_count);
        //console.log("after poping comparelist array is "+compareListArray);
        $("#" + "btn" + code).css("background-color", "#FFFFFF");
        $("#" + "btn" + code).text("Add To Compare");

    }




    return compareListArray;
}

function setStoreData() {
    productListdataStoreBackUp = {};

    //Creating the required arrays for productListdataStoreBackUp
    createFields(productListdataStoreBackUp);

    //Firstly populating different image url arrays according to image type and format

    var image_format, image_type;
    var thumbnailUrl = [];
    var tmp_arr = [];
    for (var k = 0; k < response.Result_results_images_url.length; k++) {
        if (response.Result_results_images_url[k] != null) {
            for (var l = 0; l < response.Result_results_images_url[k].length; l++) {

                image_format = response.Result_results_images_format[k][l];
                image_type = response.Result_results_images_imageType[k][l];

                if (image_format == 2 && image_type == 0) {
                    //   //////console.log("this is a primary thumbnail image");
                    var PLP_thumbnail_urls_index = response.Result_results_images_url[k][l];
                    productListdataStoreBackUp["PLP_thumbnail_urls"][k] = "http://115.112.255.4" + response.Master_results_images_url[PLP_thumbnail_urls_index];
                } else if (image_format == 1 && image_type == 0) {
                    var PDP_main_image_urls_index = response.Result_results_images_url[k][l];
                    productListdataStoreBackUp["PDP_main_image_urls"][k] = "http://115.112.255.4" + response.Master_results_images_url[PDP_main_image_urls_index];
                } else if (image_type == 1 && image_format == 2) {
                    tmp_arr.push("http://115.112.255.4" + response.Master_results_images_url[response.Result_results_images_url[k][l]]);

                }
            }
        }
        productListdataStoreBackUp["PDP_thumbnail_urls"].push(tmp_arr);
        tmp_arr = [];
    }
    initializeProductListdataStoreBackUp();
    productListdataStoreBackUp['subcategory_name'] = $('#breadcrumb li.active').text(); 
    console.log(productListdataStoreBackUp)
}

function init() {
    var t0 = window.performance.now();
    var tsession = window.performance.now();
    console.log('session token ajax call took following millisecs');
    console.log(t0 - tsession);

    var versionResponse = getJSANversion();
    var Version_number = versionResponse["Version_number"];
    response = getJSANData(Version_number);
    var tJSAN = window.performance.now();
    console.log('jsan ajax calls took following millisecs');
    console.log(tJSAN - tsession);
    //console.log(versionResponse);
    //console.log(versionResponse.Timestamp);
    console.log("Version number" + versionResponse["Version_number"]);
    // console.log("category_code"+category_code);
    console.log("Retrieving JSAN data");
    console.log(response);
    // Setting data     
    var tsetstoreData = window.performance.now();
    setStoreData();
    console.log('setStoreData() took following millisecs');
    console.log(tsetstoreData - tJSAN);


    productFilteredData = $.extend(true, [], productListdataStoreBackUp);
    //console.log("productListdataStoreBackUp is " + productListdataStoreBackUp);

    setDisplayedData(productFilteredData); // call this method to set this data as current_displayed_data
    var tproductFilteredData = window.performance.now();
    console.log('deep copy for array productFilteredData took following millisecs');
    console.log(tproductFilteredData - tsetstoreData);
    unsorted_data = $.extend(true, [], productFilteredData);
    var tunsorted_data = window.performance.now();
    console.log('deep copy for array unsorted_data took following millisecs');
    console.log(tunsorted_data - tproductFilteredData);
    mapStoreAvailabilityFacet();


    // Helpers starts from here
    // Helpers 
    Handlebars.registerHelper(
        'TotalProducts',
        function(results_code) {
            return productListdataStoreBackUp.relevance_results_name.length;
        });


    Handlebars.registerHelper('setWidthForRating', function(width) {
        ////////console.log(width);
        if (width == null) {
            return "0";
        } else {
            return "16" * width;
        }

    });

    Handlebars.registerHelper('facetValues', function(i) {
        allFacetsProductCodeIndexesUpdatedCount[i] = new Array(response.Result_facets_facets_facetResults_code[i].length);
        //console.log(response.Result_facets_facets_facetResults_name);
        return response.Result_facets_facets_facetResults_name[i];
    });

    Handlebars.registerHelper("math", function(lvalue, operator, rvalue, options) {
        lvalue = parseFloat(lvalue);
        rvalue = parseFloat(rvalue);

        return {
            "+": lvalue + rvalue,
            "-": lvalue - rvalue,
            "*": lvalue * rvalue,
            "/": lvalue / rvalue,
            "%": lvalue % rvalue
        }[operator];
    });


    Handlebars.registerHelper('equal', function(lvalue, rvalue, options) {
        if (arguments.length < 3)
            throw new Error("Handlebars Helper equal needs 2 parameters");
        if (lvalue != rvalue) {
            return options.inverse(this);
        } else {
            return options.fn(this);
        }
    });
    Handlebars.registerHelper('repeatFeatureValues', function(index, block) {
        console.log(index);
        var accum = '';
        for (var i = 0; i < response.Result_results_classifications_features_featureValues_value[0][index].length; ++i)
            accum += block.fn({ featureValueIndex: i, parentIndex: index });
        return accum;
    });




    Handlebars.registerHelper('featurevalue', function(i, j, k) {

        return response.Master_results_classifications_features_featureValues_value[response.Result_results_classifications_features_featureValues_value[i][j][k]];

    });

    Handlebars.registerHelper('facetValueCount', function(j, i) {

        return response.Master_facets_facets_facetResults_count[response.Result_facets_facets_facetResults_count[i][j]];

    });

    Handlebars.registerHelper('facetValue', function(i) {

        if (i == (response.Master_facets_facets_facetResults_name.length - 1) || i == (response.Master_facets_facets_facetResults_name.length - 2)) {
            if (response.Master_facets_facets_facetResults_name[i] == 'true') {
                return "Include Out of Stock";
            } else if (response.Master_facets_facets_facetResults_name[i] == "false") {
                return "Exclude Out of Stock";
            }
        }

        return response.Master_facets_facets_facetResults_name[i];
    });

  Handlebars.registerHelper( 'eachStoreInMap', function ( block ) {
        var city_shop = mapStoreAvailabilityFacet();
        var out = '';
        Object.keys( city_shop ).map(function( prop ) {
            out += block.fn( {key: prop, value: city_shop[ prop ]} );
        });
        return out;
    });
    /*Handlebars.registerHelper('classifications', function(i) {
        return response.Result_results_classifications_name[i];
    });

    Handlebars.registerHelper('classification', function(i) {

        return response.Master_results_classifications_name[i];
    });*/

    Handlebars.registerHelper('productDivName', function(i) {
        //  console.log(i);
        if (i % 3 == 0) {
            return "span-6  first clear";
        } else if (i % 3 == 1) {
            return "span-6";

        } else {
            return "span-6 last";
        }

    });

    var thelpers = window.performance.now();
    console.log('All helpers took following millisecs');
    console.log(thelpers - tunsorted_data);

    // since above we have initialised current data with productstoredatabackup 

    // pagination helper setting page count
    setPagesCount();
    var tsetPageCount = window.performance.now();
    console.log('setPagesCount() took following millisecs');
    console.log(tsetPageCount - thelpers);
    generatePageResults(productList_template, 'productList_template', productFilteredData);
    var tproductlistgeneration = window.performance.now();
    console.log('productlist generation took following millisecs');
    console.log(tproductlistgeneration - tsetPageCount);
    generatePageResults(pagination_template, 'pagination_template', productFilteredData);
    var tpaginationgeneration = window.performance.now();
    console.log('pagination generation took following millisecs');
    console.log(tpaginationgeneration - tproductlistgeneration);
    generatePageResults(left_bar_template, 'left_bar_template', response);
    var tleftbar = window.performance.now();
    console.log('leftbar generation took following millisecs');
    console.log(tleftbar - tpaginationgeneration);
    var tgeneratePageResults = window.performance.now();
    console.log('All generatePageResults and setPagesCount took following millisecs');
    console.log(tgeneratePageResults - tsetPageCount);
    // generatePageResults(quicksearch_template, 'quicksearch_template', productFilteredData);
    showPage(1);
    var tshowpage = window.performance.now();
    console.log('showPage() took following millisecs');
    console.log(tshowpage - tgeneratePageResults);
    makeCurrentPageNumberHighlighted(1);
    var tmakecurrentpagehighlighted = window.performance.now();
    console.log('makeCurrentPageNumberHighlighted() took following millisecs');
    console.log(tmakecurrentpagehighlighted - tshowpage);
    $(".paginationBar h1").text($('#breadcrumb li.active').text()) 
    var tinit = window.performance.now();
    console.log('init() took following millisecs');
    console.log(tinit - t0);
    //populateAllFacetsProductCodes();
    show_previousPage_facet_products();
    checboxFunctionality();
    getSessionToken();
checkSortingFilterofpreviouspage();

}
// init ends here

function setPagesCount() {
    var noOfPagesToDisplay = 5;
    Handlebars.registerHelper(
        'NumberOfPages',
        function() {
            //console.log("length of current product data : "+current_product_displayed_data.relevance_results_code.length);
            $ul = $("#pagination");
            var paginationSelector = $("#paginationdiv");
            noOfPages = Math.ceil(current_product_displayed_data.relevance_results_code.length / countLimit);
            pageArray = [];
            // page numbers to be displayed on pagination bar
            if (noOfPages <= noOfPagesToDisplay) {
                for (var i = 0; i < noOfPages; i++) {
                    pageArray[i] = i + 1;
                }
            } else {
                var fromPage = current_page_number - 2;
                var to = current_page_number + 2;
                if (fromPage <= 0) {
                    for (var i = 0; i < noOfPagesToDisplay; i++) {
                        pageArray[i] = i + 1;
                    }
                } else if (to > noOfPages) {
                    var diff = to - noOfPages;
                    fromPage = fromPage - diff;
                    for (var i = fromPage; i <= noOfPages; i++) {
                        pageArray[i] = i;
                    }
                } else {
                    for (var i = fromPage; i <= to; i++) {
                        pageArray[i] = i;
                    }
                }
            }
            
            return pageArray;
        });

}

function mapStoreAvailabilityFacet(){
    var cities = response.Result_facets_facets_facetResults_name[3];
    var final = [];
    for(var i=0;i<cities.length;i++){
        var final_city = response.Master_facets_facets_facetResults_name[cities[i]];
        var city_arr = final_city.split('z*');
        if(city_arr.length > 1){
            if(final[city_arr[0]] == undefined){
                final[city_arr[0]] = [];            
            }
            final[city_arr[0]].push(city_arr[1]);
        }
    }
    return final;
    console.log(final)
}



function getDistinctArray(arr) {
    var dups = {};
    return arr.filter(function(el) {
        var hash = el.valueOf();
        var isDup = dups[hash];
        dups[hash] = true;
        return !isDup;
    });
}

//to display the number of products added to compare list

function show_compare_count(count) {

    //////console.log("count of elements are :" + count);
    
    current_product_displayed_data["compare_count"] = count;
   
    if (compareListArray.length >= 1) 
    {
        
        var productListDataCompareList =getComparePageProductDetailsArray()
        
        
        // creating comparebox_template using data created while adding add to cart 

        generatePageResults('compare-section', 'comparebox_template', productListDataCompareList);
        
        generatePageResults(pagination_template, 'pagination_template', current_product_displayed_data);
        $("#comparePanel").show();
 if(count > 1)
        {
            document.getElementById('submitN7CompareBox').disabled = false;
        }
        else
        {
            document.getElementById('submitN7CompareBox').disabled = true;
        }
    }

    if (count == 0) 
    {
       // $("#compare_count").hide();
         $("#comparePanel").hide();
    }
replicateCompareBoxOnPDP();
}

// function to get Product Details of any product in PDP
function getProductDetails(index) {
    
var productData=getProductDetailsArray(index);
    for (var e = 0; e < response.Result_results_classifications_name[index].length; e++) {
        productData['classifications_name'].push(response.Master_results_classifications_name[response.Result_results_classifications_name[index][e]]);
    }

    Handlebars.registerHelper('features', function(i) {
        console.log(i);
        // console.log(response.Master_results_classifications_features_name[response.Result_results_classifications_features_name[productindex][i]]);
        return response.Result_results_classifications_features_name[productindex][i];

    });

    Handlebars.registerHelper('featureName', function(i) {
        console.log(i);
        console.log(response.Master_results_classifications_features_name[i]);
        return response.Master_results_classifications_features_name[i];

    });

    Handlebars.registerHelper('featureValue', function(i) {
        console.log(i);
        console.log(response.Master_results_classifications_features_featureValues_value[i]);
        return response.Master_results_classifications_features_featureValues_value[i];

    });
    var t0 = window.performance.now();
    generatePageResults(productdata_template, 'productdata_template', productData);
    console.log(productData);
  var sourceView;
    if (!($('.span-24').css('display') == 'none')){
        sourceView = 'PLP';
    $(".span-24").hide();
}
    if (!($('#comparelist-section').css('display') == 'none'))
{
    sourceView = 'Compare'
    $('#comparelist-section').hide();
}
    $("#productDetailsPanel").fadeIn("slow");
    $('#nav_main h1').css('display', 'none');
    $liBasic = $("<li class='separator'>&gt;</li><li style='padding-top: 4px;font-size: 14px'><a href=''>Advanced</a></li>");
    $liProduct = $("<li class='separator'>&gt;</li><li style='padding-top: 4px;font-size: 14px' class='active'><text style='color: #00415e;font-size: 0.9em;'>" + productListdataStoreBackUp.relevance_results_name[index] + "</text></li>")
  $(".breadcrumb ul").append($liBasic);
    $(".breadcrumb li").removeClass('active').find('text').css({"font-size": "0.9em", "color": "#919191"});;
    $(".breadcrumb ul").append($liProduct).find('li.active text').css({"font-size": "0.9em", "color": "#00415e"}); 
    var tPDP = window.performance.now();
    console.log("Showing PDP took following millisecs");
    console.log(tPDP - t0);
    $("#btnBackToPLP").click(function() {
        $("#productDetailsPanel").fadeOut("slow");
        if (sourceView == 'PLP'){
    $(".span-24").show();
}
    if (sourceView == 'Compare')
{
    $('#comparelist-section').show();
}
        $('#nav_main h1').css('display', 'none');
        $(".breadcrumb li").slice(-4).remove();
        currentPDPProductcode = "";
//var n = $('div[id^=extra]').length;
//$( ".ui-dialog" ).remove();
    });
    productData = [];

}
// get product details based on productstorebackup index
function getProductDetailsArray(index)
{ 
     var productData = $.extend(true, [], productListdataStoreBackUp);
    productData["product_categorycode"] = globalcategorycode;
    productData["product_name"] = productListdataStoreBackUp.relevance_results_name[index];
    productData["product_zoomImage_url"] = response.Master_results_url[response.Result_results_url[index]];
    productData["product_code"] = response.Master_results_code[response.Result_results_code[index]];
    productData["product_main_imageurl"] = productListdataStoreBackUp["PDP_main_image_urls"][index];
    productData["PDP_gallery_thumbnails"] = productListdataStoreBackUp["PDP_thumbnail_urls"][index];
    productData["product_price"] = productListdataStoreBackUp.relevance_price_value[index];
    productData["product_mrp"] = response.Master_results_mrp[response.Result_results_mrp[index]];
    productData["product_summary"] = productListdataStoreBackUp.relevance_results_summary[index];
    productData["product_code"] = productListdataStoreBackUp.relevance_results_code[index];
    productData["noOfReviews"] = response.Master_results_numberOfReviews[response.Result_results_numberOfReviews[index]];
    productData["averageRating"] = productListdataStoreBackUp.relevance_results_averageRating[index];
    productData["stockLevel"] = response.Master_results_numberOfReviews[response.Result_results_stock_stockLevel[index]];
    productData["stockLevelStatus"] = response.Master_results_stock_stockLevelStatus_code[response.Result_results_stock_stockLevelStatus_code[index]];
    productData["product_description"] = response.Master_results_description[response.Result_results_description[index]];
    productData['quickview'] = response.Master_results_quickviewdesc[response.Result_results_quickviewdesc[index]];
    productData['discount'] = response.Master_results_discount[response.Result_results_discount[index]];

    productData['classifications_name'] = [];
    return productData;
}
function getComparePageProductDetailsArray()
{
      var productListDataCompareList = [];
        //////console.log(productListDataCompareList);
        ////console.log(current_product_displayed_data.relevance_results_name);

        productListDataCompareList["compare_results_name"] = [];
        productListDataCompareList["compare_results_images"] = [];
        productListDataCompareList["compare_price_value"] = [];
        productListDataCompareList["compare_results_summary"] = [];
        productListDataCompareList["compare_results_averageRating"] = [];
        productListDataCompareList["compare_results_code"] = [];
        productListDataCompareList["compare_classifications_name"] = [];
        productListDataCompareList["compare_classifications_features_name"] = [];
        productListDataCompareList["compare_classifications_features_featureValues_value"] = [];
        productListDataCompareList["compare_keyFeatures"] = [];

        var allfilterindexes = [];
        for (var d = 0; d < compareListArray.length; d++) {

            var index = productListdataStoreBackUp.relevance_results_code.indexOf(compareListArray[d]);


            productListDataCompareList["compare_results_name"]
                .push(productListdataStoreBackUp.relevance_results_name[index]);
            productListDataCompareList["compare_results_images"]
                .push(productListdataStoreBackUp.relevance_results_images[index]);
            productListDataCompareList["compare_price_value"]
                .push(productListdataStoreBackUp.relevance_price_value[index]);
            productListDataCompareList["compare_results_summary"]
                .push(productListdataStoreBackUp.relevance_results_summary[index]);
            productListDataCompareList["compare_results_averageRating"]
                .push(productListdataStoreBackUp.relevance_results_averageRating[index]);
            productListDataCompareList["compare_results_code"]
                .push(productListdataStoreBackUp.relevance_results_code[index]);
            productListDataCompareList["compare_keyFeatures"].push(response.Master_results_quickviewdesc[response.Result_results_quickviewdesc[index]]);

            Handlebars.registerHelper('compare_features', function(i) {
                console.log(i);
                // console.log(response.Master_results_classifications_features_name[response.Result_results_classifications_features_name[productindex][i]]);
                return response.Result_results_classifications_features_name[index][i];

            });

            Handlebars.registerHelper('compare_featureName', function(i) {
                console.log(i);
                console.log(response.Master_results_classifications_features_name[i]);
                return response.Master_results_classifications_features_name[i];

            });

            Handlebars.registerHelper('compare_featureValue', function(i) {
                console.log(i);
                console.log(response.Master_results_classifications_features_featureValues_value[i]);
                return response.Master_results_classifications_features_featureValues_value[i];

            });
        

            allfilterindexes[d] = response.Result_results_classifications_name[index];

            console.log(allfilterindexes);
            //  console.log(getCommonElements(allfilterindexes));

            for (var f = 0; f < response.Result_results_classifications_features_name.length; f++) {
                for (var h = 0; h < (response.Result_results_classifications_features_name[f]).length; h++) {
                    productListDataCompareList["compare_classifications_features_name"].push(response.Master_results_classifications_features_name[response.Result_results_classifications_features_name[f][h]]);
                }

            }

            for (var g = 0; g < response.Result_results_classifications_features_featureValues_value.length; g++) {
                for (var l = 0; l < response.Result_results_classifications_features_featureValues_value[g].length; l++) {
                    productListDataCompareList["compare_classifications_features_featureValues_value"].push(response.Master_results_classifications_features_featureValues_value[response.Result_results_classifications_features_featureValues_value[g]]);

                }
                productListDataCompareList["compare_classifications_features_featureValues_value"].push(response.Master_results_classifications_features_featureValues_value[response.Result_results_classifications_features_featureValues_value[g]]);
            }

            // Master_results_classifications_features_featureValues_value 
            //  Master_results_classifications_name
            //  Master_results_classifications_features_name 

        }
        console.log(allfilterindexes);
        console.log(getCommonElements(allfilterindexes));
        var commonfilterindexes = getCommonElements(allfilterindexes)
        for (var k = 0; k < commonfilterindexes.length; k++) {
            productListDataCompareList["compare_classifications_name"].push(response.Master_results_classifications_name[k]);
        }
  //      productListDataCompareList['filters_length'] =  productListDataCompareList["compare_classifications_name"].length;
       // productListDataCompareList["compare_classifications_name"]=getCommonElements(allfilterindexes);
        var temp_arr = [];
        var tmp_arr =  [];
        var tmp_arr2 = [];
        var temp_arrValue = [];
        var tmp_arrValue =  [];
        var tmp_arrValue2 = [];
        //var  classification_index;
     //   var firstprodindex = productListdataStoreBackUp.relevance_results_code.indexOf(compareListArray[0]);
        //     productListDataCompareList["compare_classifications_features_name"][e] = response.Result_results_classifications_features_name[firstprodindex][e];
                  
          for (var e = 0; e < productListDataCompareList['compare_classifications_name'].length; e++) {
            for (var l = 0; l < compareListArray.length; l++) {

             var index = productListdataStoreBackUp.relevance_results_code.indexOf(compareListArray[l]);
            // var  classification_index = (response.Master_results_classifications_name).indexOf(productListDataCompareList["compare_classifications_name"][e]);
               // var features_indexes = response.Result_results_classifications_features_code[index];
               var feature_Index = response.Master_results_classifications_name.indexOf(productListDataCompareList['compare_classifications_name'][e]);
              /* if(features_indexes[e]){
                 tmp_arr[e] = response.Result_results_classifications_features_name[index][e];
                 for (var m = 0; m < tmp_arr[e].length; m++) {
                     temp_arr.push(response.Master_results_classifications_features_name[tmp_arr[e][m]]);
                 }*/
                 tmp_arr[e] = response.Result_results_classifications_features_name[index][feature_Index]; // temp_arr = subfeatures of a particular product at given index
                 tmp_arrValue[e] = response.Result_results_classifications_features_featureValues_value[index][feature_Index];
                 for (var m = 0; m < tmp_arr[e].length; m++) {
                     temp_arr.push(response.Master_results_classifications_features_name[tmp_arr[e][m]]);
                     temp_arrValue.push(response.Master_results_classifications_features_featureValues_value[tmp_arrValue[e][m]]);
                 }
                 tmp_arr2.push(temp_arr);
                 tmp_arrValue2.push(temp_arrValue);
               //  tmp_arr = [];
                 temp_arr = [];
                 temp_arrValue = [];
                 

           }   // tmp_arr.push(response.Result_results_classifications_features_featureValues_value[d][e]) 
            
    
            productListDataCompareList["compare_classifications_features_name"].push(tmp_arr2);
            productListDataCompareList["compare_classifications_features_featureValues_value"].push(tmp_arrValue2);
                        
            /* if(tmp_arr2 && tmp_arr2.length){
                console.log(getCommonElements(productListDataCompareList["compare_classifications_features_name"][e]))

        }*/
        tmp_arr2 = [];
        tmp_arrValue2 = [];
        }
     var union_subfeatures = [];
     var union_subfeatures_values = [];
        for (var p = 0; p < productListDataCompareList["compare_classifications_features_name"].length; p++) {
            if(productListDataCompareList["compare_classifications_features_name"][p].length > 0)
            {
                union_subfeatures.push(getDistinctArray([].concat.apply([], productListDataCompareList["compare_classifications_features_name"][p])));
                union_subfeatures_values.push(getDistinctArray([].concat.apply([], productListDataCompareList["compare_classifications_features_featureValues_value"][p])))
                //console.log(findCommonElements(productListDataCompareList["compare_classifications_features_name"][p]));
            }
        }

        console.log('union subfeatures');
        console.log(union_subfeatures);
        console.log('union subfeatures values');
        console.log(union_subfeatures_values);

            Handlebars.registerHelper('compare_features', function(i) {
            //    console.log(i);
               return union_subfeatures[i];

    });
    // added by joun for update method only
    Handlebars.registerHelper('repeatfeautereValuesblock', function(block) {
        var accum = '';
        for (var i = 0; i < productListDataCompareList["compare_classifications_name"].length; ++i)
            accum += block.fn({ featureIndex: i });
        return accum;
     });
    
    return productListDataCompareList;
}

function updateComparePage() {

    var productListDataCompareList = getComparePageProductDetailsArray();
   
    
    console.log("productListDataCompareList is ");
    console.log(productListDataCompareList);
    var t0 = window.performance.now();
    generatePageResults('comparelist-section', 'comparelist_template', productListDataCompareList);

    $(".span-24").hide();
    $("#comparelist-section").fadeIn("slow");
    var tupdatecomparePage = window.performance.now();
    console.log("compare page updation took following millisecs");
    console.log(tupdatecomparePage - t0)
    if (compareListArray.length > 0) {
        //console.log("hiding warning!");
        $("#compare").hide();
    } else {
        $("#compare").show();
        $("#compareparent").hide();
    }
}

function showComparePageTemplate() {
    if (compareListArray.length > 1) {
        var productListDataCompareList = getComparePageProductDetailsArray();

        console.log("productListDataCompareList is ");
        console.log(productListDataCompareList);
        var t0 = window.performance.now();
        generatePageResults('comparelist-section', 'comparelist_template', productListDataCompareList);
        // $(".span-24").hide();
            if (!($('.span-24').css('display') == 'none')) {
            sourceView2 = 'PLP';
            $(".span-24").hide();
        }
        if (!($('#productDetailsPanel').css('display') == 'none')) {
            sourceView2 = 'PDP'
            $('#productDetailsPanel').hide();
        }
        $("#comparelist-section").fadeIn("slow");
        var tshowcomparePage = window.performance.now();
        console.log("Showing compare page took following millisecs");
        console.log(tshowcomparePage - t0)
    } else {
        //alert("There should be more than 1 products.");
        $("#errorMsg").text("There should be more than 1 products.");
        $("#comparelistErrorMsg").show();
        setTimeout(function() {
            $("#comparelistErrorMsg").hide();
            $("#errorMsg").text("");
        }, 10000);
    }

    createADDTOCARTOncomparePage();

} // show compare page ends here 

function GoToProductListFromCompareList() {
    $("#comparelist-section").fadeOut("slow");
    if (sourceView2 == 'PLP') {
        $(".span-24").show();
    }

    if (sourceView2 == 'PDP') {
        $('#productDetailsPanel').show();
    }

}


// page generation generic method starts here 
// this method will take the data and compile a template and then populate that with section reflected onn the homepage
function generatePageResults(target_hmtl_component, source_template_component, responseDataOnPage) {

    var content = document.getElementsByClassName(target_hmtl_component);
    var productTemplate = document.getElementById(source_template_component).innerHTML;
    var template = Handlebars.compile(productTemplate);
    content[0].innerHTML = template(responseDataOnPage);
}
// page generation generic method ends here 

function getIntersect(arr1, arr2)

{
    var temp = [];
    for (var i = 0; i < arr1.length; i++) {
        for (var k = 0; k < arr2.length; k++) {
            if (arr1[i] == arr2[k]) {
                temp.push(arr1[i]);
                break;
            }
        }
    }

    return temp;
}

// this is a constant used to append an array while checking uniqueness
Array.prototype.unique = function() {
    var a = this.concat();
    for (var i = 0; i < a.length; ++i) {
        for (var j = i + 1; j < a.length; ++j) {
            if (a[i] === a[j])
                a.splice(j--, 1);
        }
    }

    return a;
};
// this method will take pindex and postion in the array where you are goint to inser the data 
// idea is union_array [0]will contain the pcode of same facet value like "brands" so postion will remain same till checkboxes of same category is selected 
function setUnion(pindex, position) {


    // //////console.log("pindex is "+pindex +" storing this position is  "+position);
    // //////console.log("union_array is :"+union_array);       
    var temp;
    if (union_array[position] != null) {

        temp = union_array[position];
    } else {
        temp = pindex;
    }

    if (union_array != null) {
        temp = temp.concat(pindex).unique();

    }

    union_array[position] = temp;


}
// this method will take intersection of the arrays starring from 0
// union_array [0] intrsection union_array[1] intersection union_array[2]
function getIntersectionofArrayofUnionArray(union_array) {
    var temp_intersection = [];
    for (var i = 0; i < union_array.length; i++) {

        if (i == 0) {
            temp_intersection = union_array[0]; // initialaising the temp array 
        }

        temp_intersection = getIntersect(temp_intersection, union_array[i]); // get intersection of two two arrays 

    }
    return temp_intersection;

}

function setTotalProductCount(newcount) {
    Handlebars.registerHelper(
        'TotalProducts',
        function() {
            return newcount;
        });

}

function setDisplayedData(data) {
    current_product_displayed_data = []; // removing old data 
    current_product_displayed_data = $.extend(true, [], data); // setting this data as current product displayed data
    $(current_product_displayed_data.relevance_results_code).each(function(i, element1) {

        $(compareListArray).each(function(i, element2) {
            if (element1 == element2.code) {

                addToCompare(element2);
            }
        });

    });
}

$(document).ready(function() {
$('#breadcrumb li.active').text()
$(".paginationBar h1").text($('#breadcrumb li.active').text()) 

    /*$('#search').keypress(function()
    {
        //alert('search button enabling');
        $('.siteSearchSubmit').attr('disabled',false);
    });*/

}); // document ready ends here 
function checboxFunctionality() {
    var $checkboxes = $('#divFacets li input[type="checkbox"]');

    $checkboxes.change(function() {
        var t0 = window.performance.now();
        var product_code_of_already_added_to_compare_list;
        var filtereddata = $.extend(true, [], productListdataStoreBackUp);
        var outerloop = [];
        // pcode = [];
        //pindex = [];
        productCodeIndexes = [];
        var outer_indexes_array = [];
        var intersction = false;
        var count = 0;
        var last_var_pushed = -1;
        //   checkedpcodes = [];
        current_checked_indexes_innerouterloopcommaseperatedArray = [];
        current_page_checked_product_names = [];
        checked_productCodeIndexes = [];
        // this method will populate data based on each check box selected irrescpective of brand or category
        $checkboxes.filter(':checked').each(function(index) {

            var current_checkbox_element = this;
            var facet_flag_positions = ["0," + (response.Master_facets_facets_name.length - 1)];
            //console.log( index + " node value : " + $(this)[index].nextSibling.nodeValue);
            //console.log( index + " node id: " + $(this)[index].parentNode.id+' current_checkbox_element'+current_checkbox_element);
            if (facet_flag_positions.includes($(current_checkbox_element).val())) {
                $(facet_flag_positions).each(function(i, element) {
                    console.log('facetflag position is: ' + element);
                    console.log('value of this checkbox is' + $(current_checkbox_element).val());
                    console.log('facet index of parent' + (response.Master_facets_facets_name.length - 1));
                    console.log('split last value' + element.split(",")[1]);
                    if (element.split(",")[1] == (response.Master_facets_facets_name.length - 1)) {
                        // get the text of the label which will be the value of flag if true different jsan will be fetched;
                        isflag = (current_checkbox_element.parentNode.id == 'Include Out of Stock');
                        console.log('flag now is: ' + isflag);
                        // value of flag acan be true or false but to know it is pressed clicked_flag is sused

                        clicked_flag = true;
                        //break;
                    }

                    // $(this)[index].nextSibling.nodeValue=facet_flag_name[i];

                });
            } // facet is a one of the flags condition ends here 
            current_checked_indexes_innerouterloopcommaseperatedArray.push($(this).val().trim());
            var facet_name_value_array = $(this).val().split(",");

            var i = facet_name_value_array[1].trim();
            var j = facet_name_value_array[0].trim();
            current_page_checked_product_names.push(response.Master_facets_facets_facetResults_code[response.Result_facets_facets_facetResults_code[i][j]]);
            productCodeIndexes = response.Result_facets_facets_facetResults_products[i][j];

            // till now got the productCodeindexes  now push that into the union array 

            // if more than one facet is selected then push that in outer array
            if ($.inArray(i, outerloop) == -1) {
                outerloop.push(i); // setting number of outer category selected 

                if (outerloop.length > 1) {
                    // if i is different and more than one facet is selected then intersection of those twoo arrays should take place 
                    intersction = true;
                    // count here is storing the postion where a union will be stored 
                }
                current_selected_checkbox_count = outerloop.length;
            }

            if (intersction) {
                if (last_var_pushed == i) {

                    setUnion(productCodeIndexes, count);
                } else {
                    setUnion(productCodeIndexes, ++count);
                }

            } else {
                setUnion(productCodeIndexes, count);
            }

            productCodeIndexes = [];
            last_var_pushed = outerloop[outerloop.length - 1];

        }); // checked each funtion ends here 



        if (intersction) {

            productCodeIndexes = getIntersectionofArrayofUnionArray(union_array);

        } else {

            productCodeIndexes = union_array[0];
        }



        checked_productCodeIndexes = $.extend(true, [], productCodeIndexes);
        console.log(checked_productCodeIndexes);
        console.log(checked_productCodeIndexes);
        // generate a page with above populate data
        filtereddata["relevance_results_name"] = [];
        filtereddata["relevance_results_images"] = [];
        filtereddata["relevance_price_value"] = [];
        filtereddata["relevance_results_summary"] = [];
        filtereddata["relevance_results_averageRating"] = [];
        filtereddata["relevance_results_code"] = [];

        if (productCodeIndexes != null) {

            setTotalProductCount(productCodeIndexes.length);
            //genarate pagination
            // getsorted
            //  initializeProductListdataStoreBackUp();
            //  //////console.log(productListdataStoreBackUp.relevance_results_name[0]);
            for (var d = 0; d < productCodeIndexes.length; d++) {
                ////////console.log("d is"+d);
                filtereddata["relevance_results_name"]
                    .push(productListdataStoreBackUp.relevance_results_name[productCodeIndexes[d]]);
                filtereddata["relevance_results_images"]
                    .push(productListdataStoreBackUp.relevance_results_images[productCodeIndexes[d]]);
                filtereddata["relevance_price_value"]
                    .push(productListdataStoreBackUp.relevance_price_value[productCodeIndexes[d]]);
                filtereddata["relevance_results_summary"]
                    .push(productListdataStoreBackUp.relevance_results_summary[productCodeIndexes[d]]);
                filtereddata["relevance_results_averageRating"]
                    .push(productListdataStoreBackUp.relevance_results_averageRating[productCodeIndexes[d]]);
                filtereddata["relevance_results_code"]
                    .push(productListdataStoreBackUp.relevance_results_code[productCodeIndexes[d]]);

            }

            if ($checkboxes.filter(':checked').length > 0) {
                generatePageResults(productList_template, 'productList_template', filtereddata);
                generatePageResults(pagination_template, 'pagination_template', filtereddata);
                setDisplayedData(filtereddata); // this method sets the data as current_displayed_product_data and coompare logic as well
                unsorted_data = $.extend(true, [], filtereddata);
            }
        } else {
            setTotalProductCount(productListdataStoreBackUp.relevance_results_name.length);
            generatePageResults(productList_template, 'productList_template', productListdataStoreBackUp);
            generatePageResults(pagination_template, 'pagination_template', productListdataStoreBackUp);
            setDisplayedData(productListdataStoreBackUp); // call this method to set this data as current_displayed_data
            unsorted_data = $.extend(true, [], productListdataStoreBackUp);

        }

        union_array = [];
        filtereddata = []; //clearing the resources
        //console.log(compareListArray);
        if (compareListArray.length > 0) {

            console.log("compareListArray length is > 0");
            $(compareListArray).each(function(i, element) {
                $("#" + "btn" + element).css("background-color", "orange");
                $("#" + "btn" + element).text("Remove From Compare List");
                show_compare_count(compareListArray.length);
            });
        }

        setPagesCount();
        generatePageResults(pagination_template, 'pagination_template', current_product_displayed_data);
        makeCurrentPageNumberHighlighted(1);
        var tfilter = window.performance.now();
        console.log('Filtering took following millisecs');
        console.log(tfilter - t0);
        updateFacetCount(checked_productCodeIndexes, 4); // 4 stands include out of stock
        var tupdateCount = window.performance.now();
        console.log('facet count updation took following millisecs')
        console.log(tupdateCount - tfilter); // 0 stands here for stores that is the position for stores in facet count

        if (clicked_flag) {
            clicked_flag = false;
            $("#productGrid").fadeOut("slow");
            init();
            previous_page_checked_fileters_indexes = [];
        }
           replicatePaginationBottom();
    }); // check box checked ends here 
    if (previous_page_checked_fileters_indexes.length > 0) {

        var t0 = window.performance.now();
        var product_code_of_already_added_to_compare_list;
        var filtereddata = $.extend(true, [], productListdataStoreBackUp);
        var outerloop = [];
        // pcode = [];
        //pindex = [];
        productCodeIndexes = [];
        var outer_indexes_array = [];
        var intersction = false;
        var count = 0;
        var last_var_pushed = -1;
        //   checkedpcodes = [];
        current_checked_indexes_innerouterloopcommaseperatedArray = [];
        checked_productCodeIndexes = [];
        // this method will populate data based on each check box selected irrescpective of brand or category
        $checkboxes.filter(':checked').each(function(index) {

            var current_checkbox_element = this;
            var facet_flag_positions = ["0," + (response.Master_facets_facets_name.length - 1)];
            //console.log( index + " node value : " + $(this)[index].nextSibling.nodeValue);
            // console.log( index + " node id: " + $(this)[index].parentNode.id+' current_checkbox_element'+current_checkbox_element);
            if (facet_flag_positions != $(this).val().trim()) {
                current_checked_indexes_innerouterloopcommaseperatedArray.push($(this).val().trim());
                var facet_name_value_array = $(this).val().split(",");

                var i = facet_name_value_array[1].trim();
                var j = facet_name_value_array[0].trim();
                productCodeIndexes = response.Result_facets_facets_facetResults_products[i][j];
                //productCodeIndexes=previous_page_checked_pcodes;
                // till now got the productCodeindexes  now push that into the union array 

                // if more than one facet is selected then push that in outer array
                if ($.inArray(i, outerloop) == -1) {
                    outerloop.push(i); // setting number of outer category selected 

                    if (outerloop.length > 1) {
                        // if i is different and more than one facet is selected then intersection of those twoo arrays should take place 
                        intersction = true;
                        // count here is storing the postion where a union will be stored 
                    }
                    current_selected_checkbox_count = outerloop.length;
                }

                if (intersction) {
                    if (last_var_pushed == i) {

                        setUnion(productCodeIndexes, count);
                    } else {
                        setUnion(productCodeIndexes, ++count);
                    }

                } else {
                    setUnion(productCodeIndexes, count);
                }

                productCodeIndexes = [];
                last_var_pushed = outerloop[outerloop.length - 1];
            } else {
                var checkbox_id = "checkbox_" + facet_flag_positions;
                //$(checkbox_id).prop('checked', true);
                document.getElementById(checkbox_id.trim()).checked = false;
            }

        }); // checked each funtion ends here 



        if (intersction) {

            productCodeIndexes = getIntersectionofArrayofUnionArray(union_array);

        } else {

            productCodeIndexes = union_array[0];
        }



        checked_productCodeIndexes = $.extend(true, [], productCodeIndexes);
        console.log(checked_productCodeIndexes);
        console.log(checked_productCodeIndexes);
        // generate a page with above populate data
        filtereddata["relevance_results_name"] = [];
        filtereddata["relevance_results_images"] = [];
        filtereddata["relevance_price_value"] = [];
        filtereddata["relevance_results_summary"] = [];
        filtereddata["relevance_results_averageRating"] = [];
        filtereddata["relevance_results_code"] = [];

        if (productCodeIndexes != null) {

            setTotalProductCount(productCodeIndexes.length);

            for (var d = 0; d < productCodeIndexes.length; d++) {
                ////////console.log("d is"+d);
                filtereddata["relevance_results_name"]
                    .push(productListdataStoreBackUp.relevance_results_name[productCodeIndexes[d]]);
                filtereddata["relevance_results_images"]
                    .push(productListdataStoreBackUp.relevance_results_images[productCodeIndexes[d]]);
                filtereddata["relevance_price_value"]
                    .push(productListdataStoreBackUp.relevance_price_value[productCodeIndexes[d]]);
                filtereddata["relevance_results_summary"]
                    .push(productListdataStoreBackUp.relevance_results_summary[productCodeIndexes[d]]);
                filtereddata["relevance_results_averageRating"]
                    .push(productListdataStoreBackUp.relevance_results_averageRating[productCodeIndexes[d]]);
                filtereddata["relevance_results_code"]
                    .push(productListdataStoreBackUp.relevance_results_code[productCodeIndexes[d]]);

            }

            if ($checkboxes.filter(':checked').length > 0) {
                generatePageResults(productList_template, 'productList_template', filtereddata);
                generatePageResults(pagination_template, 'pagination_template', filtereddata);
                setDisplayedData(filtereddata); // this method sets the data as current_displayed_product_data and coompare logic as well
                unsorted_data = $.extend(true, [], filtereddata);
            }
        } else {
            setTotalProductCount(productListdataStoreBackUp.relevance_results_name.length);
            generatePageResults(productList_template, 'productList_template', productListdataStoreBackUp);
            generatePageResults(pagination_template, 'pagination_template', productListdataStoreBackUp);
            setDisplayedData(productListdataStoreBackUp); // call this method to set this data as current_displayed_data
            unsorted_data = $.extend(true, [], productListdataStoreBackUp);

        }

        union_array = [];
        filtereddata = []; //clearing the resources
        //console.log(compareListArray);
        if (compareListArray.length > 0) {

            console.log("compareListArray length is > 0");
            $(compareListArray).each(function(i, element) {
                $("#" + "btn" + element).css("background-color", "orange");
                $("#" + "btn" + element).text("Remove From Compare List");
                show_compare_count(compareListArray.length);
            });
        }

        setPagesCount();
        generatePageResults(pagination_template, 'pagination_template', current_product_displayed_data);
        makeCurrentPageNumberHighlighted(1);
        var tfilter = window.performance.now();
        console.log('Filtering took following millisecs');
        console.log(tfilter - t0);
        updateFacetCount(checked_productCodeIndexes, 4); // 0 stands here for stores that is the position for stores in facet count
        var tupdateCount = window.performance.now();
        console.log('facet count updation took following millisecs')
        console.log(tupdateCount - tfilter); // 0 stands here for stores that is the position for stores in facet count
        showHideIncludeExcludeFlag();
    } // next page chekboxes issue  ends here 

    if (compareListArray.length == 0) {
        $('#compare_count').hide();
    }

  //  makeCurrentPageNumberHighlighted();

}
init();

// pagination logic starts here 
function setPaginatedData(data) {
    var temp = []; // removing old data 
    temp = $.extend(true, [], data);
    $(temp.relevance_results_code).each(function(i, element1) {

        $(compareListArray).each(function(i, element2) {
            if (element1 == element2.code) {

                addToCompare(element2);
            }
        });

    });
}

function showPage(pageNumber) {
    var old_pageNo = current_page_number;
    current_page_number = pageNumber;
    //console.log("inside show 'PAge");
    var paginatedData = $.extend(true, [], current_product_displayed_data);
    // cleaning the old data 
    paginatedData["relevance_results_name"] = [];
    paginatedData["relevance_results_images"] = [];
    paginatedData["relevance_price_value"] = [];
    paginatedData["relevance_results_summary"] = [];
    paginatedData["relevance_results_averageRating"] = [];
    paginatedData["relevance_results_code"] = [];
    var startIndex, endIndex;
    if (pageNumber == 1) {
        startIndex = 0;
    } else {
        startIndex = pageNumber * countLimit - countLimit;
    }
    endIndex = pageNumber * countLimit - 1;
    var counter = 0;
    for (var i = startIndex; i < endIndex; i++) {
        if (current_product_displayed_data.relevance_results_name[i] != null) {
            paginatedData["relevance_results_name"][counter] = (current_product_displayed_data.relevance_results_name[i]);
            paginatedData["relevance_results_images"][counter] = (current_product_displayed_data.relevance_results_images[i]);
            paginatedData["relevance_price_value"][counter] = (current_product_displayed_data.relevance_price_value[i]);
            paginatedData["relevance_results_summary"][counter] = (current_product_displayed_data.relevance_results_summary[i]);
            paginatedData["relevance_results_averageRating"][counter] = (current_product_displayed_data.relevance_results_averageRating[i]);
            paginatedData["relevance_results_code"][counter] = (current_product_displayed_data.relevance_results_code[i]);
            //copying the data from currentdata to paginated data 
            counter++;
        }

    }
    //console.log("data after pagination relevance_results_code :"+paginatedData.relevance_results_code);
    if (noOfPages > 5) {
        setPagesCount();
        generatePageResults(pagination_template, 'pagination_template', paginatedData);
    }

    generatePageResults(productList_template, 'productList_template', paginatedData);
    setPaginatedData(paginatedData);
    showAddedProductsOnAnyPage();

    makeCurrentPageNumberHighlighted(old_pageNo); // we are calling this method here as this will remove css property from old page number and automatically applies css on new page number
    if (pageNumber == -1) {
        generatePageResults(productList_template, 'productList_template', current_product_displayed_data);
        setPaginatedData(current_product_displayed_data);
        showAddedProductsOnAnyPage(current_product_displayed_data);
        makeCurrentPageNumberHighlighted(1);
    }
prodArray3d=paginatedData["relevance_results_code"].join(",");
load3dPLP('produtcode',1,1);
    replicatePaginationBottom();
    

}

function showAddedProductsOnAnyPage() {
    if (compareListArray.length > 0) {

        console.log("compareListArray length is > 0");
        $(compareListArray).each(function(i, element) {
            $("#" + "btn" + element).css("background-color", "#00bfff");
            $("#" + "btn" + element).text("Remove");
            show_compare_count(compareListArray.length);
        });
    }
}

function navPage_right() {
    if (current_page_number < noOfPages) {
        showPage(current_page_number + 1);
    }
}

function navPage_left() {
    if (current_page_number > 1) {
        showPage(current_page_number - 1);
    }
}



//Pranita : methods by me are added below


//sortProduct method called when sort option is selected
function sortProduct() {
    $("#compare_count").hide();
    var value = $("#sortOptions1").val();
    console.log("inside sorting function");
    console.log(value);
    applySorting(value, current_product_displayed_data);
    console.log("on change");
    //console.log(response)
    // console.log(productListdataStore.Master_results_name);
    generatePageResults(pagination_template, 'pagination_template', current_product_displayed_data);
    $('select[name^="sort"] option[value=' + value + ']').attr("selected", "selected");
    $("#compare_count").hide();
    var compareCount = document.getElementById("compare_count");
    var count = 0;
    if (compareCount != null) {
        count = compareCount.innerText;
        count = count.split(' ');
    }
    if (count[1] != 0 && count[1] != '') {
        $("#compare_count").show();
    }
};

function sortData(temp) {
    var indexExist = {};
    temp.relevance_results_name
        .map(function(d, i) {
            var exist = false;
            current_product_displayed_data.relevance_results_name
                .map(function(rel_data, rel_idx) {
                    if (indexExist[rel_idx]) {} else {
                        if (d == rel_data && exist == false) {
                            indexExist[rel_idx] = true;
                            exist = rel_idx;
                        }
                    }
                });
            if (exist != false) {
                temp.relevance_results_images[i] = current_product_displayed_data.relevance_results_images[exist];
                // temp.relevance_results_images[i] = current_product_displayed_data.relevance_results_images[exist];
                temp.relevance_price_value[i] = current_product_displayed_data.relevance_price_value[exist];
                temp.relevance_results_summary[i] = current_product_displayed_data.relevance_results_summary[exist];
                temp.relevance_results_averageRating[i] = current_product_displayed_data.relevance_results_averageRating[exist];
                temp.relevance_results_code[i] = current_product_displayed_data.relevance_results_code[exist];
            }
        });
    setDisplayedData(temp);
    showPage(1);
}
//Actual sorting logic goes here
function applySorting(data, obj) {
    console.log("here goes the sorting logic");
    // initializeProductListdataStoreBackUp();
    var temp = [];
    temp = $.extend(true, [], current_product_displayed_data);
    //temp.PLP_thumbnail_urls = [];
    if (data == "name-asc") {
        temp.relevance_results_name.sort();
        sortData(temp);
        //setPaginatedData(temp);
        temp = [];

    } else if (data == "relevance") {

        temp = $.extend(true, [], unsorted_data);
        console.log("setting new unsorted data naes are : " + temp.relevance_results_name);
        setDisplayedData(temp);
        showPage(1);
        makeCurrentPageNumberHighlighted(1);
        temp = [];
    } else if (data == "name-desc") {
        temp.relevance_results_name.sort().reverse();
        //temp.relevance_results_name.reverse();
        sortData(temp);

        temp = [];
        console.log("descending");

    } else if (data == 'price-asc') {
        function sortPrice(a, b) {
            return a - b;
        }
        var numArray = temp.relevance_price_value;
        numArray.sort(sortPrice);
        var indexExist = {};
        //for (var x = 0; x < numArray.length; x++) {
        numArray.map(function(d, i) {
            var exist = false;
            current_product_displayed_data.relevance_price_value
                .map(function(rel_data, rel_idx) {
                    if (indexExist[rel_idx]) {} else {
                        if (d == rel_data && exist == false) {
                            indexExist[rel_idx] = true;
                            exist = rel_idx;
                        }
                    }
                });
            if (exist != false) {
                temp.relevance_results_images[i] = current_product_displayed_data.relevance_results_images[exist];
                // temp.relevance_results_images[i] = current_product_displayed_data.relevance_results_images[exist];
                temp.relevance_results_name[i] = current_product_displayed_data.relevance_results_name[exist];
                temp.relevance_price_value[i] = current_product_displayed_data.relevance_price_value[exist];
                temp.relevance_results_summary[i] = current_product_displayed_data.relevance_results_summary[exist];
                temp.relevance_results_averageRating[i] = current_product_displayed_data.relevance_results_averageRating[exist];
                temp.relevance_results_code[i] = current_product_displayed_data.relevance_results_code[exist];
            }
        });
        setDisplayedData(temp);
        showPage(1);
        temp = [];

    } else if (data == 'price-desc') {
        function sortPrice(a, b) {
            return b - a;
        }
        var numArray = temp.relevance_price_value;
        numArray.sort(sortPrice);
        var indexExist = {};
        //for (var x = 0; x < numArray.length; x++) {
        numArray.map(function(d, i) {
            var exist = false;
            current_product_displayed_data.relevance_price_value
                .map(function(rel_data, rel_idx) {
                    if (indexExist[rel_idx]) {} else {
                        if (d == rel_data && exist == false) {
                            indexExist[rel_idx] = true;
                            exist = rel_idx;
                        }
                    }
                });
            if (exist != false) {
                temp.relevance_results_images[i] = current_product_displayed_data.relevance_results_images[exist];
                //temp.relevance_results_images[i] = current_product_displayed_data.relevance_results_images[exist];
                temp.relevance_results_name[i] = current_product_displayed_data.relevance_results_name[exist];
                temp.relevance_price_value[i] = current_product_displayed_data.relevance_price_value[exist];
                temp.relevance_results_summary[i] = current_product_displayed_data.relevance_results_summary[exist];
                temp.relevance_results_averageRating[i] = current_product_displayed_data.relevance_results_averageRating[exist];
                temp.relevance_results_code[i] = current_product_displayed_data.relevance_results_code[exist];
            }
        });
        setDisplayedData(temp);
        showPage(1);
        temp = [];
        console.log(productListdataStoreBackUp.Master_results_name);
        console.log(productListdataStore.Master_results_name);
    } else if (data == 'topRated') {
        function sortRating(a, b) {
            if (b == '!*%#') {
                b = 0;
            }
            if (a == '!*%#') {
                a = 0;
            }
            return b - a;
        }
        var numArray = temp.relevance_results_averageRating;
        numArray.sort(sortRating);
        var indexExist = {};
        numArray.map(function(d, i) {
            var exist = false;
            current_product_displayed_data.relevance_results_averageRating
                .map(function(rel_data, rel_idx) {
                    if (indexExist[rel_idx]) {} else {
                        if (d == rel_data && exist == false) {
                            indexExist[rel_idx] = true;
                            exist = rel_idx;
                        }
                    }
                });
            if (exist != false) {
                temp.relevance_results_images[i] = current_product_displayed_data.relevance_results_images[exist];
                // temp.PLP_thumbnail_urls[i] = current_product_displayed_data.relevance_results_images[exist];
                temp.relevance_results_name[i] = current_product_displayed_data.relevance_results_name[exist];
                temp.relevance_price_value[i] = current_product_displayed_data.relevance_price_value[exist];
                temp.relevance_results_summary[i] = current_product_displayed_data.relevance_results_summary[exist];
                temp.relevance_results_averageRating[i] = current_product_displayed_data.relevance_results_averageRating[exist];
                temp.relevance_results_code[i] = current_product_displayed_data.relevance_results_code[exist];
            }
        });
        setDisplayedData(temp);
        showPage(1);
        temp = [];
    }

    previous_page_sorting_selected_option=data; // handling sorting option with include out of stock

} // apply sorting ends here 

function makeCurrentPageNumberHighlighted(pageNumber) {
    var pno = "#page_";
    // here page number is the old page number  
    pno += pageNumber;
    // remove old page css properies 
    $(pno).css("font-weight", "normal");
    $(pno).css('text-decoration', 'none');
    $(pno).css('font-size', '14px');
    // changing new page css  properties 
    pno = "#page_";
    pno += current_page_number;
    //console.log("highlighting paginations  "+pno);
    $(pno).css('font-weight', 'bold')
        //$(pno).css('text-decoration', 'underline');
        // $(pno).css('font-size', '20px');

}

// Facet count logic starts here

function updateFacetCount(checkedPCodesOfFacets, dontcheckindex) {
    if (checked_productCodeIndexes.length > 0) {
        for (var i = 0; i < response.Result_facets_facets_facetResults_products.length; i++) {

            for (var j = 0; j < response.Result_facets_facets_facetResults_products[i].length; j++) {
                // apply facet count

                allFacetsProductCodeIndexesUpdatedCount[i][j] = getIntersect(checked_productCodeIndexes, response.Result_facets_facets_facetResults_products[i][j]).length;
                var name = "check_" + j + "," + i;
                // $(name).text(""+allFacetsProductCodesUpdatedCount[i][j]);
                // document.getElementById(name).style.display ='block';
                //    console.log('i : '+i+' j is :'+j); 
                if (!(i == 3) || !(i == 4)) {
                    {
                        document.getElementById(name).innerHTML = "(" + allFacetsProductCodeIndexesUpdatedCount[i][j] + ")|";
                    }
                }


            }
            // console.log("allFacetsProductCodesUpdatedCount" + allFacetsProductCodeIndexesUpdatedCount);
        }

    }

}

function hideClass(id) {
    if ($('#' + id + '').is(":checked")) {
        $('.' + id + '').show();
    } else {
        $('.' + id + '').hide();
    }
}

function toggle(obj) {

    console.log(obj.className);
    var parent = obj.closest('div');
    console.log('parent is below ');
    console.log(parent);
    var child = $(parent).find('a');
    console.log('child is below');
    console.log(child);
    var nextdiv = $(parent).next();
    console.log(nextdiv);

    //console.log($(parent).next());
    //console.log($(child).next());
    if (obj.className == "refinementToggle close") { //$(".refinementToggle").toggleClass('refinementToggle');
        $(child).removeClass('refinementToggle close');
        $(child).addClass('refinementToggle');

        $(nextdiv).show();
    } else {
        //$(".refinementToggle").toggleClass('refinementToggle close');
        $(child).removeClass('refinementToggle');
        $(child).addClass('refinementToggle close');
        $(nextdiv).hide();
    }



}

function showALLDataInPagination() {
    // we are just increasing the count limit to 100
    console.log("inside unpaginated data");
    //countLimit=$('body').attr('max_item_limit');
    showPage(-1);
    $("#paginationdiv").hide();
$("#bottomPagination").hide();

}
// for handling flags replace operation 
function getReplaced_url() {


}

//csrf token logic starts here 
var csrf_token = null;
//var token_api = window.location.origin+"/nitrogenated/csrf-token";
function getSessionToken() {
    if (csrf_token == null) {
        $.ajax({
            async: false,
            type: 'GET',
            url: "/nitrogenated/csrf-token",
            success: function(token) {
                console.log("csrf token is ");
                console.log(token.csrf);
                csrf_token = token.csrf;
            },
            complete: function(xhr, textStatus) {
                //console.log(xhr);
            }
        }).done(function(token) {
            //console.log("Inside ajax call to csrf token url");
            console.log("token is " + token);
        });
    }
    $('.CSRFToken').val(csrf_token);

    return csrf_token;
}

function buyNow(productCode) {
    var productId = $('#productId').val();
    $.ajax({
        async: false,
        type: "POST",
        url: "/cart/add/buyNow",
        data: "productCode=" + productCode + "&productId=" + productId,
        success: function(response) {
            var html;
            if (response == "success") {
                document.getElementById("buyNowRedirect").click();
            } else {
                if (response == "freebieAddError") {
                    html = 'Please select freebie product';
                } else

                if (response == "quantityError") {
                    html = 'More than 5 quantities of a product are not allowed';
                } else
                if (response == "stockError") {
                    html = 'Due to insufficient stock, any more quantity can not be added.';
                } else
                if (response == "freebieQuantityError") {
                    html = 'You have more than 5 quantity of freebie product in your cart.';
                } else
                if (response == "freebieStockError1") {
                    html = 'Due to freebie unavailability , this product cant be added to cart.';
                } else
                if (response == "freebieStockError2") {
                    html = 'Due to insufficient stock of freebie product, any more quantity can not be added.';
                } else
                if (response == "priceError") {
                    html = 'This product can not be added to the cart';
                }
                $('#buyNowErrorMessagePopup').html(html);
                $('#buyNowErrorMessagePopup').show();
                $('#buyNowErrorMessagePopup').delay(2000).fadeOut();
            }
        }
    });
}

function checkPinCodeOnPDPPage(code, index) {
    // pin is already set 

    if ($('.pinbutton').val() != "") {
        console.log('pincode is set');
        $('#pincode').val($('.pinbutton').val());
        $.ajax({

            url: "/p/pincode",
            method: "GET",
            data: {
                code: code,
                pincode: $('#pincode').val()
            },
            success: function(responseCr) {
                    $('#PincodeCheckLoading').hide();

                    $('#PincodeCheckLoading').hide();
                    $('#deliveryFlagDefault').hide();
                    $('#delivery').show();
                    var html1;

                    if (responseCr[0] == "Product is available") {

                        $("#errorMessage").css({
                            'color': 'green',
                            'display': 'block'
                        });
                        html1 = responseCr[4];
                        var storeName = responseCr[5].split(",");
                        if (responseCr[5] != "") {
                            html1 += '. Also available at ';
                            html1 += '<div class="tip2"> these stores <span style="font-weight: normal;">';
                            for (var i = 0; i < storeName.length; i++) {
                                if (storeName[i] != "") {
                                    html1 += '*';
                                    html1 += storeName[i];
                                    html1 += '<br/>';
                                }
                            }
                            html1 += "</span></div>";
                            html1 += '<span style="font-weight: normal;">. No delivery charges applicable.</span>';
                        }

                        //ACC.track.successPincodeCheckForProduct(code,productListdataStoreBackUp.relevance_results_name[index],$('#pincode').val(),categorycode);
                        //ACC.track.successPincodeCheckForProductGA(code,productListdataStoreBackUp.relevance_results_name[index],$('#pincode').val(),categorycode); 

                    } else {
                        $("#errorMessage").css({
                            'color': 'red',
                            'display': 'block'
                        });
                        html1 = '<p>' + responseCr[0] + '</p>';
                        //ACC.track.failPincodeCheckForProduct(code,productListdataStoreBackUp.relevance_results_name[index],$('#pincode').val(),categorycode);
                        //ACC.track.failPincodeCheckForProductGA(code,productListdataStoreBackUp.relevance_results_name[index],$('#pincode').val(),categorycode);
                    }
                    $('#errorMessage').html(html1);
                    var html2 = '<table><tr><td>';

                } // success function ends here 

        }); // ajaxa call ends here 
    } else {
        // ask user for pin 
        console.log('pin is not set');
        //check if atp status is live
        var atpFlag = false;
        $.ajax({
            async: false,
            type: "GET",
            url: "/checkATPLiveStatus",
            success: function(responseCr) {
                if (responseCr == "Y") {
                    atpFlag = true;
                }
            }
        });

        //if atp status is live check the hidden attribute -pinchecker populated with usergeopincode is null or not
        //it will be null if user opens the site for first time or if there is no pincode in session(it can be in cookie)
        if (atpFlag) {

            if (document.cookie.indexOf("pincode") < 0) //no pincode in cookie or session i;e; for the first time
            {
                document.getElementById("pincodeDialog").style.display = "block"; //ask user for pincode
                $(".pdpPinChk").show();
                $(".pdpPin").show();
                $("#errorMessage").hide();
                //document.getElementById("pincodeCheck").style.display="none";        
                $("#pincodeCheck").show();
$('#deliveryFlagDefault').show();
            } else //pincode exists in cookie but not in session:session expired- take it from cookie, do not ask the user to re enter
            {
                var pagenc = document
                    .getElementById("pageChecker").value; //get the page type, if home do not reload
                var reloadFlag = false;
                var sessionFlag = true;
                var productPin = false;
                $
                    .ajax({
                        async: false,
                        type: "GET",
                        url: "/setPinfromCookie",
                        success: function(responseCr) {

                            if (responseCr.charAt(0) === 'c') {
                                sessionFlag = false;
                            }
                            responseCr = responseCr.substr(1);

                            if (responseCr == "asklater") //if user clicked ask later previously
                            {
                                $('#prevPin').hide();
                                $('#pincodeDiv').hide();
                                $('#laterPin').show();
                                if ((pagenc == 'PRODUCTSEARCH') || (pagenc == 'CATEGORY')

                                    || (pagenc == 'CART')) {}
                                // reloadFlag = true;
                            } else if (responseCr != "empty") {


                                var slides = document
                                    .getElementsByClassName("pinbutton");
                                for (var i = 0; i < slides.length; i++) {
                                    slides.item(i).value = responseCr;
                                }

                                // added for correcting pincode issue in PROD
                                var jumbo = document.getElementsByClassName("jumbocms");
                                if (jumbo != null) {
                                    for (var i = 0; i < jumbo.length; i++) {
                                        var jurl = jumbo.item(i).innerHTML;


                                        if (jurl != null && jurl.indexOf("q1") >= 0) {
                                            //alert(jurl);
                                            var start = jurl.substring(0, jurl.lastIndexOf("q1") + 3);
                                            //alert(start);
                                            var curl = jurl.substring(jurl.lastIndexOf("q1") + 3);
                                            //alert(curl);
                                            var ss = start + responseCr + curl.substring(curl.indexOf("&amp;"));
                                            //alert(ss);
                                            jumbo.item(i).innerHTML = ss;

                                        }
                                    }
                                }


                                $('#prevPin').hide();
                                $('#pincodeDiv').show();
                                /* if ((pagenc == 'PRODUCTSEARCH')
                                     || (pagenc == 'CATEGORY')
                                     
                                     || (pagenc == 'CART'))
                                   reloadFlag = true;*/
                                console
                                    .log("pincode set for session attribute");

                                if (responseCr != "asklater") {
                                    if (pagenc == 'CATEGORY') {
                                        $('#pincode').val(responseCr);
                                        productPin = true;
                                    }
                                }

                            }
                        }
                    });

                if (pagenc == 'CATEGORY' && productPin) {
                    console.log('host name is ' + window.location.hostname);
                    var n7url = window.location.hostname + "/p/pincode";
                    if ($('#pincode').val() != "") {
                        $.ajax({
                            async: false,
                            url: "/p/pincode",
                            method: "GET",
                            data: {
                                code: code,
                                pincode: $('#pincode').val()
                            },
                            success: function(responseCr) {
                                $('#PincodeCheckLoading').hide();

                                $('#PincodeCheckLoading').hide();
                                $('#deliveryFlagDefault').hide();
                                $('#delivery').show();
                                var html1;

                                if (responseCr[0] == "Product is available") {
                                    $(".pdpPinChk").hide();
                                    $(".pdpPin").hide();
                                    $("#errorMessage").css({
                                        'color': 'green',
                                        'display': 'block'
                                    });
                                    html1 = responseCr[4];
                                    var storeName = responseCr[5].split(",");
                                    if (responseCr[5] != "") {
                                        html1 += '. Also available at ';
                                        html1 += '<div class="tip2"> these stores <span style="font-weight: normal;">';
                                        for (var i = 0; i < storeName.length; i++) {
                                            if (storeName[i] != "") {
                                                html1 += '*';
                                                html1 += storeName[i];
                                                html1 += '<br/>';
                                            }
                                        }
                                        html1 += "</span></div>";
                                        html1 += '<span style="font-weight: normal;">. No delivery charges applicable.</span>';
                                    }

                                    //ACC.track.successPincodeCheckForProduct(code,productListdataStoreBackUp.relevance_results_name[index],$('#pincode').val(),categorycode);
                                    //ACC.track.successPincodeCheckForProductGA(code,productListdataStoreBackUp.relevance_results_name[index],$('#pincode').val(),categorycode); 

                                } else {
                                    $("#errorMessage").css({
                                        'color': 'red',
                                        'display': 'block'
                                    });
                                    html1 = '<p>' + responseCr[0] + '</p>';
                                    //ACC.track.failPincodeCheckForProduct(code,productListdataStoreBackUp.relevance_results_name[index],$('#pincode').val(),categorycode);
                                    //ACC.track.failPincodeCheckForProductGA(code,productListdataStoreBackUp.relevance_results_name[index],$('#pincode').val(),categorycode);
                                }
                                $('#errorMessage').html(html1);
                                var html2 = '<table><tr><td>';

                            }

                        }); // AJAXCALL ENDS HERE 
                    } // if ends here for pincode is empty or not
                    else {
                        $(".pdpPinChk").show();
                        $(".pdpPin").show();
                    }
                }


            }

        } // if atp ends here 

    } // else ends here 



}

function pinchangedOnPageCallPDPCHanges() {

    console.log('error message is ' + $('#errorMessage').html());
    console.log('current product code is' + currentPDPProductcode);
    console.log('current product index is ' + currentPDPProductIndex);

    pincodechange();




}

function pincodechange() {
    var userpin = $('#usersPincode').val();
    if ((userpin.length < 6) || (userpin.length > 6)) {
        $('.pinerror').show();
    } else {
        $('.pinerror').hide();
        document.getElementById("pincodeDialog").style.display = "none";
        document.getElementById("laterPin").style.display = "none";
        geopinControllerN7(userpin);

    }
}

function geopinControllerN7(userPIN) {
    $.ajax({
        async: false,
        type: "GET",
        url: "/geoPin",
        data: "userpincode=" + userPIN,
        success: function(responseCr) {

                var html;
                if (responseCr == "asklater") {
                    $('#prevPin').hide();
                    $('#pincodeDiv').hide();

                } else if (responseCr != "empty") {


                    var slides = document
                        .getElementsByClassName("pinbutton");
                    for (var i = 0; i < slides.length; i++) {
                        slides.item(i).value = responseCr;
                    }

                    // added for correcting pincode issue in PROD
                    var jumbo = document.getElementsByClassName("jumbocms");
                    if (jumbo != null) {
                        for (var i = 0; i < jumbo.length; i++) {
                            var jurl = jumbo.item(i).innerHTML;


                            if (jurl != null && jurl.indexOf("q1") >= 0) {
                                //alert(jurl);
                                var start = jurl.substring(0, jurl.lastIndexOf("q1") + 3);
                                //alert(start);
                                var curl = jurl.substring(jurl.lastIndexOf("q1") + 3);
                                //alert(curl);
                                var ss = start + responseCr + curl.substring(curl.indexOf("&amp;"));
                                //alert(ss);
                                jumbo.item(i).innerHTML = ss;

                            }
                        }
                    }


                    $('#prevPin').hide();
                    $('#pincodeDiv').show();
                    $('#pincode').val(responseCr);

                } // else if ends here 

                console.log("pincode set for session attribute");

            } // success ends here 



    }); //ajax ends here 
    if (currentPDPProductcode != "") // user changed the pin code with pdp open 
    {
        checkPinCodeOnPDPPage(currentPDPProductcode, currentPDPProductIndex);
    }
}
//changing script on n7ct
function PincodeCheckN7() {
    $("#hdTATMsg").text('');
    $("#spTATMsg").text('');
    $("#edTATMsg").text('');
    $('#PincodeCheckLoading').show();
    var pincode = $('#pincode').val();

    if (changingPinFlag) {
        ACC.dynamicyield_track.changingPIN(pincode, 'PRODUCT', window.location.href);
    }
    var code = $('#code').val();
    var categoryCode = "132";
    var productDesc = "Nikon Coolpix B700 20.3MP Digital Camera (Black)";
    //$('#deliveryFlagDefault').show();
    $('#enterMessage').hide();
    $('#errorMessage').hide();
    if (pincode == "") {
        $('#PincodeCheckLoading').hide();
        $('#enterMessage').text("Please enter a pincode");
        $("#enterMessage").css({
            'color': 'red',
            'display': 'block'

        });
        return false;
    }
    if (!(pincode > 31 && (pincode < 48 || pincode > 57))) {
        $('#PincodeCheckLoading').hide();
        $('#enterMessage').text("Please enter valid pincode");
        $("#enterMessage").css({
            'color': 'red',
            'display': 'block'

        });
        return false;
    } else {
        $('#pincode').hide();
        $('#pincodeCheck').hide();
    }
    geopinControllerN7(pincode);
    $('#PincodeCheckLoading').hide();


}

function show_previousPage_facet_products() {

    if (current_checked_indexes_innerouterloopcommaseperatedArray.length > 0) {

        var found = false;
        previous_page_checked_product_names = current_page_checked_product_names;
        if ((previous_page_checked_product_names.indexOf("true") > -1)) {
            var indexOFFlag = previous_page_checked_product_names.indexOf("true");
            previous_page_checked_product_names.splice(indexOFFlag, 1);
        }
        if ((previous_page_checked_product_names.indexOf("false") > -1)) {
            var indexOFFlag = previous_page_checked_product_names.indexOf("false");
            previous_page_checked_product_names.splice(indexOFFlag, 1);
        }
        if (previous_page_checked_product_names.length > 0) // after removing flags if some other facet exists then run this O(n^4)
        {
            for (var i = 0; i < response.Master_facets_facets_facetResults_code.length; i++) {

                for (var j = 0; j < previous_page_checked_product_names.length; j++) {
                    // check the brand name and get its index from master
                    if (response.Master_facets_facets_facetResults_code[i] == previous_page_checked_product_names[j]) {
                        // now check the index of this facet in 2d array whose index is found in master array
                        for (ii = 0; ii < response.Result_facets_facets_facetResults_code.length; ii++) {
                            for (jj = 0; jj < response.Result_facets_facets_facetResults_code[ii].length; jj++) {
                                // check if both the index matches 
                                // i is the index of master array , ii and jj is inner and outer index of results array
                                if (response.Result_facets_facets_facetResults_code[ii][jj] == i) {
                                    var checkbox_id = "checkbox_" + jj + "," + ii;
                                    document.getElementById(checkbox_id.trim()).checked = true;
                                    // break; 
                                    found = true;
                                }

                            }
                            if (found) {
                                found = false;
                                // break;
                            }

                        }

                    }
                }
            }
        }
        console.log('prevous page checked facet are :' + previous_page_checked_product_names);
        previous_page_checked_pcodes = checked_productCodeIndexes;
        previous_page_checked_fileters_indexes = current_checked_indexes_innerouterloopcommaseperatedArray;
    }

}

function showHideIncludeExcludeFlag() {
    if (document.getElementById('Exclude Out of Stock') != null) {

        document.getElementById('Include Out of Stock').style.visibility = "hidden";

    }

}

// add to cart on comapare page functionlaity 
function createADDTOCARTOncomparePage()
{
    for (var d = 0; d < compareListArray.length; d++) 
    {

        $( "#replicateCompareATC"+compareListArray[d] ).append($("#addToCartForm"+compareListArray[d]).clone(true));

    }
}

function imageHover(id)
{
       
        $(".productImage img").attr("src",$('#'+id).find('img').attr("data-primaryimagesrc"));
        $("#zoomLink, #imageLink").attr("href",$("#zoomLink").attr("data-href")+"?galleryPosition="+$('#'+id).find('img').attr("data-galleryposition"));
        $(".productThumb .thumb").removeClass("active");
        $('#'+id).addClass("active")
}

function load3dPLP(item_id, flag, multiple) {
$('.ui-dialog').remove();
    var username = "croma@cromaretail.com";
    var password = "Abcd1234@";
    var API_KEY = "AXKI20202KT";
    if (multiple == 1) {
        var item_id = prodArray3d;
    } else {
        var item_id = item_id;
    }
    $.ajax({
        type: "GET",
        url: "http://disha3d.com/API/API.php?prodid=" + item_id,
        data: {
            'user': username,
            'password': password,
            'API_KEY': API_KEY
        },
        success: function(resp) {

            if (multiple == 1) {


                var respArray = JSON.parse(resp);

                for (var i = 0; i < respArray.length; i++) {
                    resp = respArray[i]["3d_url"];
                    if (resp == 0) {} else if (resp == 2) {} else if (resp == 3) {} else if (resp == 00) {} else {
                        if (flag == 1) {
                            oldercontent = $('#3d').html();
                            $('#div3d' + respArray[i]["item_id"]).html('<img src="/_ui/desktop/common/images/3d.png" style="cursor:pointer;" onclick="load3dPLP(' + respArray[i]["item_id"] + ',2,0)" height="30px"> ');
                        } else if (flag == 2) {
                            $('#extraPLP').dialog({
                                modal: true,
                                resizable: true,
                                closeOnEscape: true,
                                draggable: false
                            });
                            $("#extraPLP").parent().css({
                                "width": "638px",
                                "left": "358.5px"
                            });


                            $('#extraPLP').show();

                            $('#3dPLP').html('<iframe id="3diframe" src="' + resp + '" width="617" height="480" frameBorder="0" allowfullscreen/>');


                        } else if (flag == 3) {
                            $('#full').html('<iframe src="' + resp + '" height="480px" width="600px" frameBorder="0"  />');
                        } else {
                            alert('not defined');
                        }
                    }
                }
            } else {

                if (flag == 2) {
                    $('#extraPLP').dialog({
                        modal: true,
                        resizable: true,
                        closeOnEscape: true,
                        draggable: false
                    });
                    $("#extraPLP").parent().css({
                        "width": "638px",
                        "left": "358.5px"
                    });


 $('#3dPLP').html("");
                    $('#extraPLP').show();
       var n = $('div[id^=extraPLP]').length;

                    if (n > 1) {

                        for (var i = 1; i < n; i++) {

                            $('#extraPLP').remove();
                        }
                    }
                    $('#3dPLP').html('<iframe id="3diframe" src="' + resp + '" width="617" height="480" frameBorder="0" allowfullscreen/>');


                }

            }



        }
    });
}

function replicateCompareBoxOnPDP()
{
$( "#replicateCompareBoxN7" ).empty();
    $( "#replicateCompareBoxN7" ).append($(".compare-section").clone(true));
}

function replicatePaginationBottom()
{
    $("#bottomPagination").empty();
    $("#bottomPagination").append($("#paginationdiv").clone(true));
if(pageArray.length<2)
            {
                $("#paginationdiv").hide();
                $("#bottomPagination").hide();
            }
            else
            {
                $("#paginationdiv").show();
                $("#bottomPagination").show();
            }
}
function checkSortingFilterofpreviouspage()
{
    $("#sortOptions1").val(previous_page_sorting_selected_option);
    sortProduct();

}
