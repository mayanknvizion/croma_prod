<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="shortcut icon" type="image/x-icon" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/theme-blue/images/favicon.ico" />
    
    <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/css/jquery.colorbox-1.3.16.css" />
    <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/css/jquery.bt-0.9.5.css" />
    <link rel="stylesheet" type="text/css" media="all" href="http://www.croma.com/_ui/desktop/common/blueprint/screen.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/css/jquery.ui.stars-3.0.1.custom.css" />
    <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/css/jquery.ui.autocomplete-1.8.18.css" />
    <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/addons/nitrogen/desktop/common/css/nitrogen.css" />
    <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/addons/b2ccheckoutaddon/desktop/common/css/b2ccheckoutaddon.css" />
    <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/addons/b2ccheckoutaddon/desktop/common/css/checkoutContentPanel.css" />
    <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/addons/b2ccheckoutaddon/desktop/common/css/checkoutOrderDetails.css" />
    <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/addons/b2ccheckoutaddon/desktop/common/css/checkoutProgress.css" />
    <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/css/helper.css" />
    <!-- <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/css/main.css" /> -->
    <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/css/buttons.css" />
    <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/css/forms.css" />
    <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/css/header.css" />
    <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/css/miniCart.css" />
    <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/css/siteSearch.css" />
    <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/css/navigation.css" />
    <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/css/breadcrumb.css" />
    <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/css/facetNav.css" />
    <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/css/paginationBar.css" />
    <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/css/productGrid.css" />
    <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/css/productList.css" />
    <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/css/productDetails.css" />
    <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/css/order.css" />
    <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/css/orderTotals.css" />
    <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/css/footer.css" />
    <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/css/colorBox.css" />
    <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/css/searchPOS.css" />
    <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/css/userRegister.css" />
    <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/css/userLogin.css" />
    <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/css/userGuest.css" />
    <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/css/account.css" />
    <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/css/cartItems.css" />
    <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/css/landingLayout2Page.css" />
    <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/css/storeDetail.css" />
    <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/css/storeFinder.css" />
    <link rel="stylesheet" type="text/css" media="all" href="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/theme-blue/css/changes.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="./css/font-awesome.css">
    <link rel="stylesheet" href="./css/compare.css">
    <link rel="stylesheet" href="./css/croma.css"> 
    <link rel="stylesheet" href="./css/main.css">
    <link rel="stylesheet" href="./css/ui.css">
    <link rel="stylesheet" href="./css/change.css">
    

    <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.4/handlebars.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/2.4.1/lodash.min.js"></script>
    <script src="http://www.castopulence.org/js/acc.js"></script>
    <script src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/jquery.accessible-tabs-1.9.7.min.js"></script>
    <style>
    .linkActive {
        color: red;
    }
    </style>
</head>
<body>
<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.4/handlebars.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/2.4.1/lodash.min.js"></script>
    <script src="http://www.castopulence.org/js/acc.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/acc.stars.js"></script>
    <div id="page" data-currency-iso-code="USD">
        <div id="content" class="clearfix">
            <div class="span-24">
                <section id="left-bar-section">
                </section>
                <!-- left bar template ends here  -->
                <div class="span-18 last">
                    <section id="compare-section">
                    </section>
                    <section id="pagination-section">
                    </section>
                    <section id="productList-section">
                    </section>
                </div>
            </div>
        </div>
    </div>
    <!-- productdata starts here  -->
    <section id="productdata-section" hidden>
    </section>
    <!-- productdata ends here  -->
    <!-- comparelist page starts here -->
    <section id="comparelist-section" hidden>
    </section>
    <!-- comparelist page ends here -->
    <!-- all templates definition starts from here  -->
    
    <jsp:include page="compare-template.html"></jsp:include>
    <jsp:include page="comparelist-template.html"></jsp:include>
    <jsp:include page="left_bar-template.html"></jsp:include>
    <jsp:include page="pagination-template.html"></jsp:include>
    <jsp:include page="productdata-template.html"></jsp:include>
    <jsp:include page="productlist-template.html"></jsp:include>
    
      

    
    
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript">
    /*<![CDATA[*/
    var ACC = {
        config: {}
    };
    ACC.config.contextPath = "/nitrogenstorefront";
    ACC.config.encodedContextPath = "/nitrogenstorefront/electronics/en";
    ACC.config.commonResourcePath = "/nitrogenstorefront/_ui/desktop/common";
    ACC.config.themeResourcePath = "/nitrogenstorefront/_ui/desktop/theme-blue";
    ACC.config.siteResourcePath = "/nitrogenstorefront/_ui/desktop/site-electronics";
    ACC.config.rootPath = "/nitrogenstorefront/_ui/desktop";
    ACC.config.CSRFToken = "e0d1db98-9fb9-4c8c-a185-18b0eb29e267";
    ACC.pwdStrengthVeryWeak = 'Very weak';
    ACC.pwdStrengthWeak = 'Weak';
    ACC.pwdStrengthMedium = 'Medium';
    ACC.pwdStrengthStrong = 'Strong';
    ACC.pwdStrengthVeryStrong = 'Very strong';
    ACC.pwdStrengthUnsafePwd = 'password.strength.unsafepwd';
    ACC.pwdStrengthTooShortPwd = 'Too short';
    ACC.pwdStrengthMinCharText = 'Minimum length is %d characters';
    ACC.accessibilityLoading = 'Loading... Please wait...';
    ACC.accessibilityStoresLoaded = 'Stores loaded';
    ACC.autocompleteUrl = '/nitrogenstorefront/electronics/en/search/autocomplete';
    /*]]>*/
    </script>
    <script type="text/javascript">
    /*<![CDATA[*/
    ACC.addons = {}; //JS holder for addons properties
    ACC.addons.b2ccheckoutaddon = [];
    ACC.addons.b2ccheckoutaddon['key.1'] = 'value 1';
    ACC.addons.b2ccheckoutaddon['key.2'] = 'value 2';
    /*]]>*/
    </script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/jquery.query-2.1.7.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/jquery-ui-1.8.18.min.js"></script>
    <!--<script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/jquery.jcarousel-0.2.8.min.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/jquery.tmpl-1.0.0pre.min.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/jquery.blockUI-2.39.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/jquery.colorbox.custom-1.3.16.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/jquery.slideviewer.custom.1.2.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/jquery.waitforimages.min.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/jquery.scrollTo-1.4.2-min.js"></script>
    
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/jquery.form-3.09.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/jquery.bgiframe-2.1.2.min.js"></script>
    [if IE]><script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/excanvas-r3.compiled.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/jquery.bt-0.9.5-rc1.min.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/jquery.pstrength.custom-1.2.0.js"></script>
    <script type="text/javascript" src="http://static.addtoany.com/menu/page.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/acc.common.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/acc.userlocation.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/acc.track.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/acc.cms.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/acc.product.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/acc.refinements.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/acc.storefinder.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/acc.carousel.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/acc.autocomplete.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/acc.pstrength.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/acc.password.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/acc.minicart.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/acc.pickupinstore.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/acc.userlocation.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/acc.langcurrencyselector.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/acc.paginationsort.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/acc.checkout.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/acc.cartremoveitem.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/acc.address.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/acc.refresh.js"></script>-->
<!--     <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/acc.stars.js"></script> -->
    <!--<script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/acc.forgotpassword.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/jquery.accessible-tabs-1.9.7.min.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/acc.productDetail.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/acc.producttabs.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/cms/shareonsocialnetworkaction.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/cms/addtocartaction.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/cms/pickupinstoreaction.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/addons/nitrogen/desktop/common/js/nitrogen.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/addons/b2ccheckoutaddon/desktop/common/js/b2ccheckoutaddon.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/addons/b2ccheckoutaddon/desktop/common/js/acc.checkoutcartdetails.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/addons/b2ccheckoutaddon/desktop/common/js/acc.checkoutpickupdetails.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/addons/b2ccheckoutaddon/desktop/common/js/acc.deliverymodedescription.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/addons/b2ccheckoutaddon/desktop/common/js/acc.hopdebug.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/addons/b2ccheckoutaddon/desktop/common/js/acc.payment.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/addons/b2ccheckoutaddon/desktop/common/js/acc.placeorder.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/addons/b2ccheckoutaddon/desktop/common/js/acc.removepaymentdetails.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/addons/b2ccheckoutaddon/desktop/common/js/acc.silentorderpost.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/addons/b2ccheckoutaddon/desktop/common/js/acc.updatebillingaddress.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/addons/b2ccheckoutaddon/desktop/common/js/acc.termsandconditions.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/addons/b2ccheckoutaddon/desktop/common/js/acc.checkoutaddress.js"></script>
    <script type="text/javascript" src="http://ny2.nviz.info:9001/nitrogenstorefront/_ui/desktop/common/js/acc.skiplinks.js"></script>-->
    <script src="scripts.js"></script>

</body>
</html>